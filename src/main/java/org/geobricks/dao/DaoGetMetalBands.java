package org.geobricks.dao;

import org.geobricks.Data.Band;

import java.sql.*;
import java.util.ArrayList;

public class DaoGetMetalBands {
    public static ResultSet GetBands() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connect = DriverManager.getConnection("jdbc:mysql://localhost/metal?"
                        + "user=jeffrey&password=some_pass");
        Statement statement = connect.createStatement();
        return statement.executeQuery("select * from metal.bands");
    }

    public static void AddBand(String name, String type, String start, String end, String location) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connect = DriverManager.getConnection("jdbc:mysql://localhost/metal?"
                + "user=jeffrey&password=some_pass");
        PreparedStatement statement = connect.prepareStatement( String.format("INSERT INTO bands VALUES ('%s','%s','%s','%s','%s')",name,type,start,end,location));
        statement.execute();
    }

    public static ArrayList<Band> getMetalBandByName(String name) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connect = DriverManager.getConnection("jdbc:mysql://localhost/metal?"
                + "user=jeffrey&password=some_pass");
        Statement statement = connect.createStatement();
        return (ArrayList<Band>) statement.executeQuery("select * from metal.bands where name = '" + name +"'");
    }
}
