package org.geobricks.service;

import com.google.gson.Gson;
import org.geobricks.Data.Band;
import org.geobricks.dao.DaoGetMetalBands;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GetMetalBands {

    public static String GetBandsHTML() {
        ArrayList<Band> bands = GetBands();
        return ConvertBandsToHTMLGrid(bands);
    }

    private static String ConvertBandsToHTMLGrid(ArrayList<Band> bands)
    {
        String output = "<table style=\"width:100%\"><tr>" +
                "<th>Name</th><th>Type</th><th>Start</th><th>End</th><th>Location</th></tr>";
        for (Band band:bands) {
            output = output+ "<tr><th>" +band.getName() +
                    "</th><th>" + band.getType() +"</th><th>" + band.getStart() +
                    "</th><th>" + band.getEnd() +"</th><th>" + band.getLocation() +
                    "</th></tr>";
        }
        return output + "</table>";
    }

    public static String GetJsonBands() {
        Gson g = new Gson();
        g.toJson(GetBands());
        return g.toJson(GetBands());
    }

    private static ArrayList<Band> GetBands() {
        try {
            ArrayList<Band> bands =  new ArrayList<>();
            ResultSet res = DaoGetMetalBands.GetBands();
            while (res.next())
            {
                Band aux = new Band();
                aux.setName(res.getString("name"));
                aux.setEnd(res.getString("end"));
                aux.setLocation(res.getString("location"));
                aux.setStart(res.getString("start"));
                aux.setType(res.getString("type"));
                bands.add(aux);
            }
            return bands;
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public static String AddBand(String name, String type, String start, String end, String location) throws SQLException, ClassNotFoundException {
        DaoGetMetalBands.AddBand(name,type,start,end,location);
        return GetBandsHTML();
    }

    public static String getMetalBandByName(String name) {
        try {
            return ConvertBandsToHTMLGrid(DaoGetMetalBands.getMetalBandByName(name));
        }
        catch (Exception e) {}
        return "There is a problem and it fails";
    }
}
