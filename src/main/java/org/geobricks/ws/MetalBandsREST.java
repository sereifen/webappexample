package org.geobricks.ws;

import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.geobricks.service.GetMetalBands;

import java.sql.SQLException;

@Component
@Path("/Metal")
public class MetalBandsREST {

    @GET
    @Path("")
    public Response getMetalsBands() {
        /* Stream result */
        return Response.status(200).entity(GetMetalBands.GetBandsHTML()).build();

    }

    @GET
    @Path("json/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMetalsBandsJson() {

        /* Stream result */
        return Response.status(200).entity(GetMetalBands.GetJsonBands()).build();

    }

    @GET
    @Path("addMetalBand")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addMetalBand(@QueryParam("name") String name,
                                 @QueryParam("type") String type,
                                 @QueryParam("start") String start,
                                 @QueryParam("end") String end,
                                 @QueryParam("location") String location) throws SQLException, ClassNotFoundException {

        /* Stream result */
        return Response.status(200).entity(GetMetalBands.AddBand(name,type,start,end,location)).build();

    }


    @GET
    @Path("getMetalBandByName")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMetalBandByName(@QueryParam("name") String name) throws SQLException, ClassNotFoundException {

        /* Stream result */
        return Response.status(200).entity(GetMetalBands.getMetalBandByName(name)).build();

    }

}